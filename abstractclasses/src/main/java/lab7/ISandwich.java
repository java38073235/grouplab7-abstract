package lab7;

public interface ISandwich {
    public String getFilling();
    public void addFilling(String topping);
    public boolean isVegetarian();
}
